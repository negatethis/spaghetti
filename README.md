Spaghetti - Static Blog Generator
=========================

This is the source code for a simple static site generator written in Python using ```jinja2``` and ```markdown2```.

Why?
-----

I've visited a lot of people's personal sites and was inspired to make my own. I decided to program my own static site generator because I figured it would be a good project to learn how to code a bit, to control my site as much as possible, and because I want my site to be as simple as possible and follow the philosophies of a simpler web, although my ideas of "simpler web" have changed with my exposure to the Gemini protocol.

There's no "why" to why I chose the name.


 How to Use
--------------

1. Make sure you have both ```jinja2``` and ```markdown2``` installed by running:

    ```
    pip3 install jinja2

    pip3 install markdown2
    ```

2. Edit the files in the ```templates/``` folder with your own site information.

3. Place any static files you'll be using, like media or CSS styles, into the ```site/static/``` folder.

4. Create the content you'll want to add to your site and place it into the ```content/``` folder as a Markdown file. 

    The Markdown files in ```content/``` must have certain metadata at the beginning in order to format the entire page correctly.

    This should be at the beginning of every Markdown file in ```content/```:
    ```
    ---
    title: <Title of this page>
    date: <Date that the post was made. Any format you want>
    page: <EXTREMELY IMPORTANT. Options are "extra" and "post". "post" is for blog posts that should appear on the blog list page and update the RSS feed. "extra" is for any other non-blog post page>
    slug: <IMPORTANT. The file name that the HTML page will have>
    ---
    ```

    Needlessly complicated, you say? You're darn right.

5. Once you've added any new pages to ```content/``` navigate to the root of the project and either:

    Run

    ```python3 main.py```

    or

    ```chmod +x main.py``` and then ```./main```

After this, all the HTML files and the RSS XML file should be created in the ```site/``` folder. The blog page will automatically list every blog you've created and have an appropriate link to the file. There will also be an archive of the entire code and content (minus any ```.git/``` folder you may have) created at ```site/source.tar.gz```

You can navigate into the ```site/``` folder, run ```python3 -m http.server``` and go to ```127.0.0.1:8000``` in your browser to see the site.

You could also now run an Apache2 web server with ```site/``` as the DocumentRoot, or the equivalent on Nginx.

Credit
-----------------

I followed [this page](https://dev.to/nqcm/making-a-static-site-generator-with-python-part-1-3kn3) as a guide to create this static site generator. I added the RSS feed generator, the repository archiver, and the logic to distinguish between "extra" pages and "blog post" pages in order to create a blog list page separate from the index page. The rest of the code should be attributed to the creator of the guide, Naveera Ashraf, who has given me permission to share this code and license it as long as I include this attribution.

So credit to [Naveera Ashraf](https://github.com/nqcm) for providing [the source code](https://github.com/nqcm/static-site-generator) that this site generator is heavily based on and allowing me to distribute it with the listed changes. Thank you.

Extra
------

I realize that the part of the code that archives the entire repository is sort of redundant since people can host their site's content on a git server that allows for downloading an archive of the source code. However, that part of the code is there because my original plan was to host my personal website's code on a static git page using [stagit](https://git.codemadness.org/stagit/), and automatically archiving the site's source code would make distributing it much easier.
