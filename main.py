#!/usr/bin/python3

import os
import tarfile
from jinja2 import Environment, PackageLoader
from markdown2 import markdown

# holds all pages
POSTS = {}

# will holds all blog pages to be added to blog list page
BLOGS = {}
count = 1

# Grabs all markdown files in content/ and adds 
# their contents and metadata to POSTS 
for post in os.listdir('content'):
        file_path = os.path.join('content', post)
        with open(file_path, 'r') as file:
            POSTS[post] = markdown(file.read(), extras=['metadata'])
    
POSTS = {
    post: POSTS[post] for post in POSTS     
}

# Loads templates/ and passes template data into two variables
env = Environment(loader = PackageLoader('main', 'templates'))
blog_page_template = env.get_template('blog.html')
post_template = env.get_template('post.html')

for post in POSTS:
    
    # Looks specifically for blog posts, 
    if POSTS[post].metadata['page'] == 'post':
        post_metadata = POSTS[post].metadata
        
        post_data = {
            'content': POSTS[post],
            'title': post_metadata['title'],
            'date': post_metadata['date'],
        }
        
        post_html = post_template.render(post = post_data)
        
        post_file_path = 'site/{slug}.html'.format(slug = post_metadata['slug'])
        
        # Rendered blog posts are added into site/
        with open(post_file_path, 'w') as file:
            file.write(post_html)
            
        # All blog posts titles, dates, and slugs are passed
        # into BLOGS to render the blog list page later
        BLOGS.update({count : {'title' : post_metadata['title'], 'slug' : post_metadata['slug'], 'date' : post_metadata['date'], 'content' : POSTS[post]}})
        
        count += 1
    
    # Looks for anything that isn't a blog post 
    # (e.g. index.html or about.html)
    if POSTS[post].metadata['page'] == 'extra':
        extra_post_metadata = POSTS[post].metadata
        
        extra_post_data = {
            'content': POSTS[post],
            'title': extra_post_metadata['title'],
        }
        
        extra_post_html = post_template.render(post = extra_post_data)
        
        extra_post_file_path = 'site/{slug}.html'.format(slug = extra_post_metadata['slug'])
        
        # Rendered pages are added into site/
        with open(extra_post_file_path, 'w') as file:
            file.write(extra_post_html)

# Blog list page is rendered by itself with
# BLOGS containing all the titles, dates, and
# slugs of the blog posts
blog_html = blog_page_template.render(posts = BLOGS)
with open('site/blog.html', 'w') as file:
    file.write(blog_html)

# Load atom feed template and render it with the same
# blogs list as we render the blog list page
atom_xml = env.get_template("atom.xml").render(posts = BLOGS)
with open('site/atom.xml', 'w') as file:
    file.write(atom_xml)

# The entire program (minus the .git folder) 
# is compressed to generate a tar file for 
# distributing source code (NOTE: this could be 
# moved into .git/hooks/post-receive
# as a bash script but I don't know bash.)
try:
    with tarfile.open("site/source.tar.gz", "w:gz") as tar:
        for path, subdirs, files in os.walk("."):
            subdirs[:] = [d for d in subdirs if d != '.git']
            for item in files:
                tar.add((path + "/{}".format(item)))
except FileExistsError:
    os.remove("site/source.tar.gz")
